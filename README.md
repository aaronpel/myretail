# MyRetail Product Page

## Project Highlights
I built this product page using **React**. The state is managed using **Redux**.

### Functionality
* Styling is done mobile-first. Classes used to style the app are managed with `./src/data-helpers/Classes.js`, loading css files from `./src/styles/index.js`
* ProductData (including `title` and `price`), Photos, and Quantity are managed by the redux reducers found in `./src/store/reducers/`
* Logic for Purchasing Options is handled by the `switch` in `./src/components/DetailsPurchasing.jsx`
* My image carousel is partially in the css, but mostly contained within `./src/components/TitlePhotos.jsx` and `./src/store/reducers/Photos.js`

### Testing
* `./src/components/DetailsPurchasing.test.jsx`
  * Testing a stateless component for rendering it's intended markup
* `./src/components/DetailsQuantity.test.jsx`
  * Testing simulated events (`change` and `click`), as well as spying on a function for how many times it is called and with which paramiters
* `./src/store/reducers/Quantity.test.jsx` 
  * Testing how a redux reducer mutates the state with checks on data types and a few edge cases

## Deployment & Hosting
Install [yarn](https://yarnpkg.com/en/) if you do not already have it installed (`npm` should work fine as well), then from Terminal, run:

```
git clone git@bitbucket.org:aaronpel/myretail.git
cd myretail
yarn
yarn build
```

Then host the files in the `./build/` directory using any http server. [Create React App](https://github.com/facebook/create-react-app) comes with one build in, if you want to use that. Since this is only frontend code, you could host it on something like AWS CloudFront for multi-location hosting.

Future iterations could be made on this codebase to improve it and make it viable for other sets of product data, or pull product data from a remote data source. Idealy if this were a real project, we would have more tests for better code coverage.

### Development Server

For development mode, run `yarn start` instead of `yarn build`

## Rough Project Outline

### Components Breakdown
* Product (`<App/>`)
  * `<TitlePhotos/>`
  * `<Details />`
    * `<DetailsPrice/>`
    * `<DetailsPromotions/>`
    * `<DetailsQuantity/>`
    * `<DetailsPurchasing/>`
    * `<DetailsHighlights/>`
  * `<Reviews />`
    * `<ReviewContent/>` (One for Pro and one for Con)
    * `<ReviewsStars/>` (Helper component used in a few places)

### Helper Code
* StripHTML(str)

### Redux
#### Reducers
* ProductData (obj, {item-data.json})
* Quantity (num)
* Photos (obj, { images:[], position: 0 })

#### Actions
* ProductDataRefresh()
* QuantitySet(num amt)
* QuantityAdjust(num amt)
* PhotosSetImages(arr images)
* PhotosSlidePosition(num newPosition)
* PhotosSwapPrimary(num arrayKey)

---

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
