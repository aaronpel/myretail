/**
 * Manage all html classes used by this application
 */
const wrapper = "wrapper"
const button = "button"
const ButtonsWrapper = "buttons-wrapper"

export default {
  /** First-level wrapper components */
  WrapperDetails: `${wrapper} wrapper-details`,
  WrapperPhotos: `${wrapper} wrapper-photos`,
  WrapperReviews: `${wrapper} wrapper-reviews`,

  /** Images */
  ImagePrimary: "image-primary",
  ImageSlider: "image-slider",

  /** Buttons */
  ButtonStorePickup: `${button} button-store-pickup`,
  ButtonAddToCart: `${button} button-add-to-cart`,
  ButtonSmall: `${button} button-small`,

  /** Text */
  TextTiny: "text-tiny",
  TextSmall: "text-small",
  TextLarge: "text-large",
  TextHidden: "text-hidden",
  TextRight: "text-right",

  /** Second-level components */
  Discounts: "discounts",
  Quantity: "quantity",
  Promotions: "promotions",
  Purchasing: "purchasing",
  Sharing: `${ButtonsWrapper} sharing`,
  Highlights: "highlights",

  /** Stars */
  StarGroup: "star-group",
  StarFilled: "star-filled",
  StarEmpty: "star-empty",

  /** Misc. */
  PurchasingOptions: `${ButtonsWrapper} purchasing-options`,
  Returns: "returns",
  SideHeading: "side-heading",
  NextPrevArrows: "next-prev-arrows",
  ReviewsHeading: "reviews-heading",
  ReviewsBody: "review-body",
  ReviewContent: "review-content",
  BottomBorder: "bottom-border",
}
