/** Import all stylesheets in their proper order */
import "./index.css"
import "./images.css"
import "./buttons.css"
import "./details.css"
import "./reviews.css"

import "./responsive.css"