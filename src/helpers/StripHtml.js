/**
 * Removes all html tags from a string
 * @source https://css-tricks.com/snippets/javascript/strip-html-tags-in-javascript/
 * @param {str} OriginalString the input string which may or may not include html
 * @returns the same text but with all html removed
 */
const StripHtml = (OriginalString) => {
  return OriginalString.replace(/(<([^>]+)>)/ig,"")
}

export default StripHtml