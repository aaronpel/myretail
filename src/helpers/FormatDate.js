import DateFormat from "dateformat"

const FormatDate = (date = new Date()) => {
  return DateFormat(date, "mmm d, yyyy")
}

export default FormatDate