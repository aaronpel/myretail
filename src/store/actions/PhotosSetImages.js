import ActionTypes from "./../ActionTypes"

/**
 * Disapatches an action to all reducers to set the photo image URLs array
 * @param {func} dispatch the redux dispatcher action
 * @param {arr} payload an array of image urls
 */
const PhotosSetImages = (dispatch, payload) => {
  const data = {
    type: ActionTypes.PhotosSetImages,
    payload
  }
  return dispatch(data)
}

export default PhotosSetImages
