import ActionTypes from "./../ActionTypes"

/**
 * Disapatches an action to all reducers to adjust the quantity up or down
 * @param {func} dispatch the redux dispatcher action
 * @param {num} payload of amount to adjust the quantity
 */
const QuantityAdjust = (dispatch, payload) => {
  payload = parseInt(payload)
  const data = {
    type: ActionTypes.QuantityAdjust,
    payload
  }
  return dispatch(data)
}

export default QuantityAdjust
