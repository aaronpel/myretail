import ActionTypes from "./../ActionTypes"

/**
 * Disapatches an action to all reducers to update the slideshow position
 * @param {func} dispatch the redux dispatcher action
 * @param {num} payload the key of the image we want to display first in the slideshow
 */
const PhotosSlidePosition = (dispatch, payload) => {
  payload = parseInt(payload)
  const data = {
    type: ActionTypes.PhotosSlidePosition,
    payload
  }
  return dispatch(data)
}

export default PhotosSlidePosition
