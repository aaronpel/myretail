import ActionTypes from "./../ActionTypes"

/**
 * Disapatches an action to all reducers to swap the values of the
 *  first (array key of 0) image with another image that is not already at key 0
 * @param {func} dispatch the redux dispatcher action
 * @param {num} payload the key of the image we want to swap with the first image
 */
const PhotosSwapPrimary = (dispatch, payload) => {
  payload = parseInt(payload)
  const data = {
    type: ActionTypes.PhotosSwapPrimary,
    payload
  }
  return dispatch(data)
}

export default PhotosSwapPrimary
