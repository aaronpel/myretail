import ActionTypes from "./../ActionTypes"

/**
 * Disapatches an action to all reducers to set the quantity
 * @param {func} dispatch the redux dispatcher action
 * @param {num} payload amount to set the quantity to
 */
const QuantitySet = (dispatch, payload) => {
  payload = parseInt(payload)
  const data = {
    type: ActionTypes.QuantitySet,
    payload
  }
  return dispatch(data)
}

export default QuantitySet
