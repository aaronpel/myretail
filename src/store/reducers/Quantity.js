import ActionTypes from "./../ActionTypes"
import DefaultStates from "./../DefaultStates"

const defaultState = DefaultStates.Quantity

/**
 * Manages a product's quantity value. Minimum value of 1, Maximum of 999.
 * @param {obj} state the current state of Quantity
 * @param {obj} data
 *    @param {str} type the action type
 *    @param {num} payload the number used to mutate state
 * @returns the new state of Quantity
 */
const Quantity = (state = defaultState, { type, payload }) => {
  if (typeof payload === "number") {
    switch(type) {
    default:
      /** No update */
      break

    case ActionTypes.QuantityAdjust:
      state = state + payload
      break

    case ActionTypes.QuantitySet:
      state = payload
      break
    }
  }

  if (isNaN(state) || state < 1) {
    state = 1
  }

  if(state > 999) {
    state = 999
  }

  return state
}

export default Quantity