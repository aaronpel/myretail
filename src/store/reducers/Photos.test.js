import ActionTypes from "./../ActionTypes"
import DefaultStates from "./../DefaultStates"
import Photos from "./Photos"

describe("[redux reducer] Photos", () => {
  let DefaultState, Data, NewState
  let testImages = [
    "http://example.com/image1.jpg", "http://example.com/image2.jpg",
    "http://example.com/image3.jpg", "http://example.com/image4.jpg",
    "http://example.com/image5.jpg", "http://example.com/image6.jpg",
    "http://example.com/image7.jpg", "http://example.com/image8.jpg",
  ]
  beforeEach(() => {
    DefaultState = DefaultStates.Photos
    Data = {
      type: "",
      payload: 1
    }
  })

  it("dosen't have images in it's default state", () => {
    expect(DefaultState.images.length).toBe(0)
  })

  it("[PhotosSetImages] sets images", () => {
    Data.type = ActionTypes.PhotosSetImages
    Data.payload = testImages
    NewState = Photos(DefaultState, Data)
    expect(NewState.images.length).toBe(8)
  })

  it("[PhotosSlidePosition] updates position", () => {
    /** Set photos */
    Data.type = ActionTypes.PhotosSetImages
    Data.payload = testImages
    NewState = Photos(DefaultState, Data)

    /** Update slide position */
    Data.type = ActionTypes.PhotosSlidePosition
    Data.payload = 2
    NewState = Photos(NewState, Data)
    expect(NewState.position).toBe(Data.payload)
  })

  it("[PhotoSlidePosition] is always zero when we have too few images", () => {
    /** Set only 2 photos */
    Data.type = ActionTypes.PhotosSetImages
    Data.payload = [
      "http://example.com/image1.jpg",
      "http://example.com/image2.jpg"
    ]
    NewState = Photos(DefaultState, Data)

    /** Attempt to update slide position */
    Data.type = ActionTypes.PhotosSlidePosition
    Data.payload = 2
    NewState = Photos(NewState, Data)
    expect(NewState.position).toBe(0)
  })

  it("[PhotosSwapPrimary] swaps image values", () => {
    Data.type = ActionTypes.PhotosSetImages
    Data.payload = testImages
    NewState = Photos(DefaultState, Data)
    Data.type = ActionTypes.PhotosSwapPrimary
    Data.payload = 1
    NewState = Photos(NewState, Data)
    expect(NewState.images[0]).not.toBe(testImages[0])
    expect(NewState.images[0]).toBe(testImages[1])
    expect(NewState.images[1]).toBe(testImages[0])
  })
})