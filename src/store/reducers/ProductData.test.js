import ActionTypes from "./../ActionTypes"
import DefaultStates from "./../DefaultStates"
import ProductData from "./ProductData"

describe("[redux reducer] ProductData", () => {
  let DefaultState, Data, NewState
  beforeEach(() => {
    DefaultState = DefaultStates.ProductData
    Data = {
      type: "",
      payload: 1
    }
  })

  it("has default state", () => {
    expect(typeof DefaultState).toBe("object")
    expect(DefaultState.UPC.length).toBeGreaterThanOrEqual(1)
  })

  it("[ProductDataRefresh] refreshes data", () => {
    Data.type = ActionTypes.ProductDataRefresh
    NewState = ProductData({}, Data)
    expect(DefaultState.UPC.length).toBeGreaterThanOrEqual(1)
  })
})