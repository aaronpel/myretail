import ActionTypes from "./../ActionTypes"
import DefaultStates from "./../DefaultStates"
import Quantity from "./Quantity"

describe("[redux reducer] Quantity", () => {
  let DefaultState, Data, NewState
  beforeEach(() => {
    DefaultState = DefaultStates.Quantity
    Data = {
      type: "",
      payload: 1
    }
  })

  it("[QuantityAdjust] updates correctly", () => {
    Data.type = ActionTypes.QuantityAdjust
    Data.payload = 5
    NewState = Quantity(DefaultState, Data)
    expect(NewState).toBe((DefaultState+Data.payload))
  })

  it("[QuantityAdjust] updates correctly with negative numbers", () => {
    Data.type = ActionTypes.QuantityAdjust
    DefaultState = 10
    Data.payload = -5
    NewState = Quantity(DefaultState, Data)
    expect(NewState).toBe((DefaultState+Data.payload))
  })

  it("[QuantityAdjust] does not update below 1", () => {
    Data.type = ActionTypes.QuantityAdjust
    Data.payload = -50
    NewState = Quantity(DefaultState, Data)
    expect(NewState).toBe((1))
  })

  it("[QuantityAdjust] does not update when given a string", () => {
    Data.type = ActionTypes.QuantityAdjust
    Data.payload = "5"
    NewState = Quantity(DefaultState, Data)
    expect(NewState).toBe(DefaultState)
  })

  it("[QuantitySet] updates state to be the payload", () => {
    Data.type = ActionTypes.QuantitySet
    Data.payload = Math.ceil(Math.random() * 10)
    NewState = Quantity(DefaultState, Data)
    expect(NewState).toBe(Data.payload)
  })

  it("[QuantitySet] updates to 1 instead of a negative number", () => {
    Data.type = ActionTypes.QuantitySet
    Data.payload = -5
    NewState = Quantity(DefaultState, Data)
    expect(NewState).toBe(1)
  })

  it("[QuantitySet] does not update when given a string", () => {
    Data.type = ActionTypes.QuantitySet
    Data.payload = "5"
    NewState = Quantity(DefaultState, Data)
    expect(NewState).toBe(DefaultState)
  })
})