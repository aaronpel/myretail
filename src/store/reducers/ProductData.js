import ActionTypes from "./../ActionTypes"
import DefaultStates from "./../DefaultStates"

const defaultState = DefaultStates.ProductData

/**
 * Loads product data
 * @param {obj} state the current state of ProductData
 * @param {obj} data
 *    @param {str} type the action type
 * @returns the new state of ProductData
 */
const ProductData = (state = defaultState, { type }) => {
  switch(type) {
  default:
    /** No update */
    break

  case ActionTypes.ProductDataRefresh:
    state = Object.assign({}, defaultState)
    break
  }

  return state
}

export default ProductData
