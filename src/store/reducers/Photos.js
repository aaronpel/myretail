import ActionTypes from "./../ActionTypes"
import DefaultStates from "./../DefaultStates"

const defaultState = DefaultStates.Photos

/**
 * Manages a product's main photo and additional photos slider
 * @param {obj} state the current state of Photos
 * @param {obj} data
 *    @param {str} type the action type
 *    @param {num} payload the data used to mutate state, see comments below for each action's payload
 * @returns the new state of Photos
 */
const Photos = (state = defaultState, { type, payload }) => {
  switch(type) {
  default:
    /** No update */
    break

  /**
   * @param {arr} payload An array of image URLs
   */
  case ActionTypes.PhotosSetImages:
    state = Object.assign({}, state)
    state.images = payload
    break

  /**
   * @param {num} payload The array key of the image that will become our first image of the slideshow
   */
  case ActionTypes.PhotosSlidePosition:{
    state = Object.assign({}, state)
    payload = parseInt(payload)
    let ImageCount = state.images.length
    let LastPosition = (ImageCount-4)
    if(LastPosition < 0) {
      LastPosition = 0
    }

    if(payload > -1 && payload <= LastPosition) {
      state.position = payload
    } else if (payload === -1) {
      state.position = LastPosition
    } else {
      state.position = 0
    }}
    break

  /**
   * @param {num} payload The array key of the image that will become our primary image
   */
  case ActionTypes.PhotosSwapPrimary:
    payload = parseInt(payload)
    if(payload > 0 && payload <= state.images.length) {
      state = Object.assign({}, state)
      state.images = state.images.slice()
      let NewFirstImage = state.images[payload]
      let OldFirstImage = state.images[0]
      state.images[0] = NewFirstImage
      state.images[payload] = OldFirstImage
    }
    break
  }

  if (state < 0) {
    state = 0
  }

  return state
}

export default Photos