import ProductData from "./../data-helpers/item-data"

let product = ProductData.CatalogEntryView[0]

export default {
  Photos: {
    images: [],
    position: 0
  },
  ProductData: product,
  Quantity: 1,
}
