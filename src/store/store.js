import { createStore, applyMiddleware, combineReducers } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import thunkMiddleware from "redux-thunk"

import Photos from "./reducers/Photos"
import ProductData from "./reducers/ProductData"
import Quantity from "./reducers/Quantity"

const reducers = combineReducers({
  Photos,
  ProductData,
  Quantity,
})

const initialState = {}

const store = createStore(reducers, initialState, composeWithDevTools(applyMiddleware(thunkMiddleware)))

export default store
