
export default {
  PhotosSetImages: "PhotosSetImages",
  PhotosSlidePosition: "PhotosSlidePosition",
  PhotosSwapPrimary: "PhotosSwapPrimary",
  ProductDataRefresh: "ProductDataRefresh",
  QuantityAdjust: "QuantityAdjust",
  QuantitySet: "QuantitySet",
}
