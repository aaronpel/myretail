import React from "react"

import Classes from "./../data-helpers/Classes"
import DetailsQuantity from "./DetailsQuantity"
import DetailsPrice from "./DetailsPrice"
import DetailsPromotions from "./DetailsPromotions"
import DetailsPurchasing from "./DetailsPurchasing"
import DetailsSharing from "./DetailsSharing"
import DetailsHighlights from "./DetailsHighlights"

/** Display product details section */
const Details = () => {
  return <div className={Classes.WrapperDetails}>
    <DetailsPrice />
    <DetailsPromotions />
    <DetailsQuantity />
    <DetailsPurchasing />
    <DetailsSharing />
    <DetailsHighlights />
  </div>
}

export default Details
