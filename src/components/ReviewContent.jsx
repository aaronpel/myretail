import React from "react"

import ReviewsStars from "./ReviewsStars"
import Classes from "../data-helpers/Classes"

const ReviewContent = (props) => {
  const { Review } = props
  return <section className={Classes.ReviewContent}>
    <ReviewsStars className={Classes.TextTiny} Rating={Review.Overall} />
    <strong>{Review.Title}</strong>
    <p className={Classes.TextSmall}>{Review.Message}</p>
    <p className={Classes.TextSmall}><a href={`#review/${Review.Key}`}>{Review.Name}</a> {Review.Date}</p>
  </section>
}

export default ReviewContent