import React from "react"
import toJson from "enzyme-to-json"

import { shallow } from "../helpers/testing/SetupEnzyme"
import { TestComponent } from "./DetailsQuantity"

describe("<Details />", () => {
  let Component, Props
  beforeEach(() => {
    Props = {
      dispatch: jest.fn(),
      FuncAdjust: jest.fn(),
      FuncSet: jest.fn(),
      Qty: "5",
    }
    Component = shallow(<TestComponent {...Props} />)
  })

  it("loads without crashing", () => {
    expect(typeof Component).toBe("object")
  })

  it("[FuncAdjust] is called with `-1` when - is clicked", () => {
    Component.find("IconRemoveCircle").parent().simulate("click")
    expect(Props.FuncAdjust).toBeCalledTimes(1)
    expect(Props.FuncAdjust).toBeCalledWith(Props.dispatch, -1)
  })

  it("[FuncAdjust] is called with `1` when + is clicked", () => {
    Component.find("IconAddCircle").parent().simulate("click")
    expect(Props.FuncAdjust).toBeCalledTimes(1)
    expect(Props.FuncAdjust).toBeCalledWith(Props.dispatch, 1)
  })

  it("[FuncSet] is called with `8` when input is changed", () => {
    Component.find("input#quantity-field").simulate("change", { target: { value: "8" }})
    expect(Props.FuncSet).toBeCalledTimes(1)
    expect(Props.FuncSet).toBeCalledWith(Props.dispatch, "8")
  })

  it("snapshots", () => {
    expect(toJson(Component)).toMatchSnapshot()
  })
})