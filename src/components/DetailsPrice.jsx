import React from "react"
import { connect } from "react-redux"

import Classes from "./../data-helpers/Classes"

/** Display this product's price & qualifier */
const DetailsPrice = (props) => {
  const { Price } = props
  return <div>
    <span className={Classes.TextLarge}>{Price.Amount}</span>
    <span className={Classes.TextSmall}> {Price.Qualifier}</span>
  </div>
}

const MapStateToProps = (state) => {
  /** Load Price Data */
  const Price = {
    Qualifier: state.ProductData.Offers[0].OfferPrice[0].priceQualifier,
    Amount: state.ProductData.Offers[0].OfferPrice[0].formattedPriceValue,
  }
  return {
    Price
  }
}

export default connect(MapStateToProps)(DetailsPrice)