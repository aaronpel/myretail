import React from "react"
import { connect } from "react-redux"

import ReviewsStars from "./ReviewsStars"
import ReviewContent from "./ReviewContent"
import Classes from "../data-helpers/Classes"
import StripHtml from "./../helpers/StripHtml"
import FormatDate from "./../helpers/FormatDate"

const Reviews = (props) => {
  const { Consolidated, Con, Pro, TotalReviews } = props
  return <div className={Classes.WrapperReviews}>
    <div className={Classes.ReviewsHeading}>
      <div>
        <ReviewsStars Rating={Consolidated} />
        <span> overall</span>
      </div>
      <div className={Classes.TextRight}><a href="#all-reviews">view all {TotalReviews} reviews</a></div>
    </div>
    <div className={Classes.ReviewsBody}>
      <div className={Classes.BottomBorder}>
        <div>
          <strong>PRO</strong>
          <em className={Classes.TextTiny}>most helpful 4-5 star review</em>
        </div>
        <div>
          <strong>CON</strong>
          <em className={Classes.TextTiny}>most helpful 1-2 star review</em>
        </div>
      </div>
      <div>
        <div><ReviewContent Review={Pro} /></div>
        <div><ReviewContent Review={Con} /></div>
      </div>
    </div>
  </div>
}

const MapStateToProps = (state) => {
  /**
   * Returns only the data we need from a review object with the keys for <ReviewsReview />
   * @param {obj} review the review we want to parse
   * @returns {obj} a simplified and cleansed version of the review data, and a formatted .Date
   */
  const ReviewData = (review) => {
    return {
      Key: review.reviewKey,
      Overall: review.overallRating,
      Title: review.title,
      Message: StripHtml(review.review),
      Name: review.screenName,
      Date: FormatDate(review.datePosted)
    }
  }

  const Reviews = state.ProductData.CustomerReview[0]

  const Consolidated = Reviews.consolidatedOverallRating
  const Con = ReviewData(Reviews.Con[0])
  const Pro = ReviewData(Reviews.Pro[0])
  const TotalReviews = Reviews.totalReviews

  return {
    Consolidated,
    Con,
    Pro,
    TotalReviews,
  }
}

export default connect(MapStateToProps)(Reviews)