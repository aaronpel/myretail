import React from "react"
import { connect } from "react-redux"

import PhotoSetImages from "../store/actions/PhotosSetImages"
import PhotosSlidePosition from "../store/actions/PhotosSlidePosition"
import PhotosSwapPrimary from "../store/actions/PhotosSwapPrimary"
import Classes from "../data-helpers/Classes"

import IconChevronLeft from "./../icons/IconChevronLeft"
import IconChevronRight from "./../icons/IconChevronRight"

/**
 * Display this product's title, primary photo, and photo slideshow.
 */
const TitlePhotos = (props) => {
  const { dispatch, ProductTitle, NeedsInitalization, Images, SlideshowPosition } = props

  if(NeedsInitalization) {
    PhotoSetImages(dispatch, Images)
  }

  /** Note: I am using inline styles here, since JS needs these elements to be specific dimensions,
   * and I am controling the slide position with javascript.
   */
  const SliderStyles = {
    SliderWrapper: { width: "9rem" },
    List: { left: `${-(SlideshowPosition*3)}rem` },
    ListItem: { width: "3rem", height: "3rem" },
  }

  return <div className={Classes.WrapperPhotos}>
    <h1>{ProductTitle}</h1>
    <div className={Classes.ImagePrimary}>
      <img src={Images[0]} alt="Main product" />
    </div>
    <div className={Classes.ImageSlider}>
      <span className={Classes.NextPrevArrows} onClick={() => {PhotosSlidePosition(dispatch, (SlideshowPosition-1))}}><IconChevronLeft /></span>
      <div style={SliderStyles.SliderWrapper}>
        <ul style={SliderStyles.List}>
          {Images.map((url, arrayKey) => {
            let returnJSX = ""
            if(arrayKey > 0) {
              returnJSX = <li key={arrayKey} style={SliderStyles.ListItem}>
                <img src={url} onClick={()=>{PhotosSwapPrimary(dispatch, arrayKey)}} alt={`Alternate product #${arrayKey}`}  />
              </li>
            }
            return returnJSX
          })}
        </ul>
      </div>
      <span className={Classes.NextPrevArrows} onClick={() => {PhotosSlidePosition(dispatch, (SlideshowPosition+1))}}><IconChevronRight /></span>
    </div>
  </div>
}

const MapStateToProps = (state) => {
  let Images = []
  let SlideshowPosition = 0
  let NeedsInitalization = false
  const ProductTitle = state.ProductData.title

  if(state.Photos.images.length > 0) {
    Images = state.Photos.images
    SlideshowPosition = state.Photos.position
  } else {
    /** Attempt to get images from product data */
    const ProductImages = state.ProductData.Images[0]

    const PrimaryImage = ProductImages.PrimaryImage[0].image
    const AlternateImages = ProductImages.AlternateImages
  
    Images.push(PrimaryImage)
    for(let photo of AlternateImages) {
      Images.push(photo.image)
    }
    /**
     * If we found images, set NeedsInitalization to true so we
     * import them into the Photos reducer from within our component
     * */
    if(Images.length > 0) {
      NeedsInitalization = true
    }
  }

  return {
    ProductTitle,
    NeedsInitalization,
    Images,
    SlideshowPosition,
  }
}

export default connect(MapStateToProps)(TitlePhotos)
