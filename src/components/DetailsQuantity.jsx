import React from "react"
import { connect } from "react-redux"

import QuantityAdjust from "./../store/actions/QuantityAdjust"
import QuantitySet from "./../store/actions/QuantitySet"
import Classes from "../data-helpers/Classes"

import IconAddCircle from "./../icons/IconAddCircle"
import IconRemoveCircle from "./../icons/IconRemoveCircle"

let DetailsQuantity = (props) => {
  const { dispatch, FuncSet, FuncAdjust, Qty } = props

  const addOne = () => { FuncAdjust(dispatch, 1) }
  const subtractOne = () => { FuncAdjust(dispatch, -1) }
  const setQty = (num) => { FuncSet(dispatch, num.replace(/\D/g, "")) }

  return <div className={Classes.Quantity}>
    <label htmlFor="quantity-field"><span className={Classes.TextSmall}>Quantity</span></label>
    <span onClick={subtractOne} aria-label="Remove one quantity" ><IconRemoveCircle /></span>
    <input type="number" id="quantity-field" value={Qty} onChange={(e)=>{setQty(e.target.value)}} />
    <span onClick={addOne} aria-label="Add one quantity"><IconAddCircle /></span>
  </div>
}

DetailsQuantity.defaultProps = {
  FuncAdjust: QuantityAdjust,
  FuncSet: QuantitySet,
}

const MapStateToProps = (state) => {
  return {
    Qty: state.Quantity
  }
}

export const TestComponent = DetailsQuantity
export default connect(MapStateToProps)(DetailsQuantity)