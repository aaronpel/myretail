import React, { Fragment } from "react"
import { connect } from "react-redux"

import Classes from "./../data-helpers/Classes"

/** Display this product's purchasing options based on the purchasingChannelCode  */
const DetailsPurchasing = (props) => {
  const { PurchasingChannel, Quantity, ReturnDays = 30 } = props
  let JsxOptions

  const Pickup = <span className={Classes.ButtonStorePickup}>
    <a href="#pickup">Pick up in store</a>
    <div className={Classes.TextSmall}>find in a store</div>
  </span>

  const Cart = <span className={Classes.ButtonAddToCart} onClick={()=>{console.log(`adding ${Quantity} to your cart.`)}}>
    <a href="#cart" >Add to cart</a>
  </span>

  switch(PurchasingChannel) {
  default:
    JsxOptions = <Fragment />
    break
  case "0":
    JsxOptions = <Fragment>{Pickup}{Cart}</Fragment>
    break
  case "1":
    JsxOptions = <Fragment>{Cart}</Fragment>
    break
  case "2":
    JsxOptions = <Fragment>{Pickup}</Fragment>
    break
  }
  return <div className={Classes.Purchasing}>
    <div className={Classes.PurchasingOptions}>{JsxOptions}</div>
    
    <div className={Classes.Returns}>
      <span className={Classes.SideHeading}>returns</span>
      <span className={Classes.TextTiny}>This item must be returned within {ReturnDays} days of the ship date. See return policy for details. Prices, promotions, styles, and availability may vary by store and online.</span>
    </div>
  </div>
}

const MapStateToProps = (state) => {
  /** Load Price Data */
  const PurchasingChannel = state.ProductData.purchasingChannelCode

  /** Get the current visitor-set quantity */
  const Quantity = state.Quantity

  /** I could not find the copy for returns in the item-data.json file, so I only pulled out the days for a "Regular Guest" */
  const ReturnDays = state.ProductData.ReturnPolicy[0].ReturnPolicyDetails[0].policyDays
  return {
    PurchasingChannel,
    Quantity,
    ReturnDays,
  }
}

export const TestComponent = DetailsPurchasing
export default connect(MapStateToProps)(DetailsPurchasing)