import React from "react"
import { connect } from "react-redux"

import Classes from "./../data-helpers/Classes"
import StripHtml from "./../helpers/StripHtml"

/** Display this product's features */
const DetailsHighlights = (props) => {
  const { Features } = props
  return <div className={Classes.Highlights}>
    <h2>product highlights</h2>
    <ul className={Classes.TextSmall}>
      {Features.map((feature, k)=>{
        return <li key={k}>
          {StripHtml(feature)}
        </li>
      })}
    </ul>
  </div>
}

const MapStateToProps = (state) => {
  return {
    Features: state.ProductData.ItemDescription[0].features,
  }
}

export default connect(MapStateToProps)(DetailsHighlights)