import React from "react"
import toJson from "enzyme-to-json"

import { shallow } from "../helpers/testing/SetupEnzyme"
import { TestComponent } from "./DetailsPurchasing"
import Classes from "../data-helpers/Classes"

describe("<Details />", () => {
  let Component, Props
  const FindPickup = (Component) => {
    return Component.find(`span[className="${Classes.ButtonStorePickup}"]`).length
  }
  const FindCart = (Component) => {
    return Component.find(`span[className="${Classes.ButtonAddToCart}"]`).length
  }
  beforeEach(() => {
    Props = { PurchasingChannel: "0" }
  })

  it("loads without crashing", () => {
    Component = shallow(<TestComponent {...Props} />)
    expect(typeof Component).toBe("object")
  })

  it("[Pickup In Store] and [Add to Cart] purchasing options when [PurchasingChannel = '0']", () => {
    Component = shallow(<TestComponent {...Props} />)
    expect(FindPickup(Component)).toBe(1)
    expect(FindCart(Component)).toBe(1)
  })

  it("[Add to Cart] is the only purchasing options when [PurchasingChannel = '1']", () => {
    Props.PurchasingChannel = "1"
    Component = shallow(<TestComponent {...Props} />)
    expect(FindPickup(Component)).toBe(0)
    expect(FindCart(Component)).toBe(1)
  })

  it("[Pickup In Store] is the only purchasing options when [PurchasingChannel = '2']", () => {
    Props.PurchasingChannel = "2"
    Component = shallow(<TestComponent {...Props} />)
    expect(FindPickup(Component)).toBe(1)
    expect(FindCart(Component)).toBe(0)
  })

  it("snapshots with both elements", () => {
    Component = shallow(<TestComponent {...Props} />)
    expect(toJson(Component)).toMatchSnapshot()
  })

})