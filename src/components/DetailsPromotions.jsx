import React from "react"
import { connect } from "react-redux"

import Classes from "./../data-helpers/Classes"

import IconLocalOffer from "../icons/IconLocalOffer"

/** Display this product's promotions */
const DetailsPromotions = (props) => {
  const { Promotions } = props
  let returnJSX = ""
  if(Promotions.length > 0) {
    returnJSX = <ul className={Classes.Promotions}>
      {Promotions.map((promo, k) => <li key={k}>
        <IconLocalOffer /> {promo.short}
      </li>)}
    </ul>
  }
  return returnJSX
}

const MapStateToProps = (state) => {
  /** Load Promotional Data */
  let Promotions = []
  if(state.ProductData.Promotions.length > 0) {
    Promotions = state.ProductData.Promotions.reduce((returnArray, promo) => {
      /**
       * Note: I assume that the promotion start & end dates should be ignored since they
       * are for 2014, and this text appears in the photoshop layout
       */
      let newData = {
        short: promo.Description[0].shortDescription,
        legal: promo.Description[0].legalDisclaimer
      }
      returnArray.push(newData)
      return returnArray
    }, [])
  }
  
  return {
    Promotions
  }
}

export default connect(MapStateToProps)(DetailsPromotions)
