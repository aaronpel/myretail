import React from "react"

import Classes from "../data-helpers/Classes"
import IconStar from "./../icons/IconStar"

/**
 * Helper component designed to take in a number and display that amount out of 5 stars
 * @param {num} Rating The number of filled in stars
 */
const ReviewsStars = (props) => {
  const { Rating = 0 } = props
  const NumStars = parseInt(Rating)
  
  return <span className={Classes.StarGroup}>
    <span className={(NumStars>0?Classes.StarFilled:Classes.StarEmpty)}><IconStar /></span>
    <span className={(NumStars>1?Classes.StarFilled:Classes.StarEmpty)}><IconStar /></span>
    <span className={(NumStars>2?Classes.StarFilled:Classes.StarEmpty)}><IconStar /></span>
    <span className={(NumStars>3?Classes.StarFilled:Classes.StarEmpty)}><IconStar /></span>
    <span className={(NumStars>4?Classes.StarFilled:Classes.StarEmpty)}><IconStar /></span>
  </span>
}

export default ReviewsStars