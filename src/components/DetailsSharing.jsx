import React from "react"

import Classes from "../data-helpers/Classes"

/** Display this product's price & qualifier */
const DetailsSharing = () => {
  return <div className={Classes.Sharing}>
    <span className={Classes.ButtonSmall}><a href="#registry">Add to Registry</a></span>
    <span className={Classes.ButtonSmall}><a href="#list">Add to List</a></span>
    <span className={Classes.ButtonSmall}><a href="#share">Share</a></span>
  </div>
}

export default DetailsSharing