import React, { Component } from "react"

import Details from "./components/Details"
import Reviews from "./components/Reviews"
import TitlePhotos from "./components/TitlePhotos"

class App extends Component {
  render() {
    return (
      <section className="App">
        <TitlePhotos />
        <Details />
        <Reviews />
      </section>
    )
  }
}

export default App