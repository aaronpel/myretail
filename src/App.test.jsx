import React from "react"
import toJson from "enzyme-to-json"

import App from "./App"
import { shallow } from "./helpers/testing/SetupEnzyme"

describe("<Details />", () => {
  let Component
  beforeEach(() => {
    Component = shallow(<App />)
  })

  it("loads without crashing", () => {
    expect(typeof Component).toBe("object")
  })

  it("snapshots", () => {
    expect(toJson(Component)).toMatchSnapshot()
  })
})